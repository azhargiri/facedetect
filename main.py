import sys
from qtcvmain import Gui

def main():
    ex = Gui(sys.argv)
    ex.show()
    sys.exit(ex.app.exec_())

if __name__ == '__main__':
    main()
