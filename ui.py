# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainWindow.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        # Main window
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(517, 543)
        MainWindow.setStyleSheet(_fromUtf8("background-color: qlineargradient(spread:pad, x1:0.994318, y1:1, x2:1, y2:0, stop:0.0284091 rgba(104, 106, 115, 255), stop:1 rgba(200, 200, 200, 255));\n"
"\n"
"selection-background-color: qlineargradient(spread:repeat, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(56, 61, 208, 214), stop:1 rgba(33, 30, 255, 255));"))

        # central widget
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))

        # window frame
        self.videoFrame = QtGui.QLabel(self.centralwidget)
        self.videoFrame.setGeometry(QtCore.QRect(10, 10, 491, 321))
        self.videoFrame.setText(_fromUtf8(""))
        self.videoFrame.setObjectName(_fromUtf8("videoFrame"))

        # window frame 2
        self.videoFrame_2 = QtGui.QLabel(self.centralwidget)
        self.videoFrame_2.setGeometry(QtCore.QRect(10, 340, 311, 101))
        self.videoFrame_2.setText(_fromUtf8(""))
        self.videoFrame_2.setObjectName(_fromUtf8("videoFrame_2"))

        # frame
        self.frame = QtGui.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(420, 340, 81, 71))
        self.frame.setStyleSheet(_fromUtf8("\n"
"background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(214, 6, 41, 255), stop:1 rgba(167, 167, 167, 165));"))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))

        # tool button 2
        self.toolButton_2 = QtGui.QToolButton(self.frame)
        self.toolButton_2.setGeometry(QtCore.QRect(10, 10, 61, 51))
        self.toolButton_2.clicked.connect(); # bind with action

        # set font style for tool button 2
        font = QtGui.QFont()
        font.setPointSize(7)
        self.toolButton_2.setFont(font)

        # Add icon to tool button 2
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("../../icon/png/computing101.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButton_2.setIcon(icon)
        self.toolButton_2.setIconSize(QtCore.QSize(32, 32))
        self.toolButton_2.setCheckable(False)
        self.toolButton_2.setAutoExclusive(False)
        self.toolButton_2.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.toolButton_2.setObjectName(_fromUtf8("toolButton_2"))

        # frame 2
        self.frame_2 = QtGui.QFrame(self.centralwidget)
        self.frame_2.setGeometry(QtCore.QRect(330, 340, 81, 71))
        self.frame_2.setStyleSheet(_fromUtf8("QFrame#frame_2{\n"
"background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(17, 212, 196, 255), stop:1 rgba(167, 167, 167, 165));\n"
"}\n"
"\n"
"\n"
""))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))

        # Tool button
        self.toolButton = QtGui.QToolButton(self.frame_2)
        self.toolButton.setGeometry(QtCore.QRect(10, 10, 61, 51))
        self.toolButton.setIcon(icon)
        self.toolButton.setIconSize(QtCore.QSize(32, 32))
        self.toolButton.setCheckable(False)
        self.toolButton.setAutoExclusive(False)
        self.toolButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.toolButton.setObjectName(_fromUtf8("toolButton"))


        # frame 3
        self.frame_3 = QtGui.QFrame(self.centralwidget)
        self.frame_3.setGeometry(QtCore.QRect(330, 420, 81, 71))
        self.frame_3.setStyleSheet(_fromUtf8("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(64, 224, 6, 255), stop:1 rgba(167, 167, 167, 165));"))
        self.frame_3.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_3.setObjectName(_fromUtf8("frame_3"))

        # Tool button 2
        self.toolButton_3 = QtGui.QToolButton(self.frame_3)
        self.toolButton_3.setGeometry(QtCore.QRect(10, 10, 61, 51))

        # add icont to Tool button 3
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8("../../icon/hd_external.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButton_3.setIcon(icon1)
        self.toolButton_3.setIconSize(QtCore.QSize(32, 32))
        self.toolButton_3.setCheckable(False)
        self.toolButton_3.setAutoExclusive(False)
        self.toolButton_3.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.toolButton_3.setObjectName(_fromUtf8("toolButton_3"))

        self.frame_2.raise_()
        self.toolButton_3.raise_()

        # frame 4
        self.frame_4 = QtGui.QFrame(self.centralwidget)
        self.frame_4.setGeometry(QtCore.QRect(420, 420, 81, 71))
        self.frame_4.setStyleSheet(_fromUtf8("background-color: qlineargradient(spread:pad, x1:1, y1:0, x2:1, y2:0, stop:0 rgba(5, 26, 214, 255), stop:1 rgba(167, 167, 167, 165));"))
        self.frame_4.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_4.setObjectName(_fromUtf8("frame_4"))

        # tool button 4
        self.toolButton_4 = QtGui.QToolButton(self.frame_4)
        self.toolButton_4.setGeometry(QtCore.QRect(10, 10, 61, 51))

        # add icon to tool button 4
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8("../../icon/Icons/Items/Stone Sword.ico")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButton_4.setIcon(icon2)
        self.toolButton_4.setIconSize(QtCore.QSize(64, 64))
        self.toolButton_4.setCheckable(False)
        self.toolButton_4.setAutoExclusive(False)
        self.toolButton_4.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.toolButton_4.setObjectName(_fromUtf8("toolButton_4"))

        # text edit
        self.textEdit = QtGui.QTextEdit(self.centralwidget)
        self.textEdit.setGeometry(QtCore.QRect(10, 450, 311, 41))
        self.textEdit.setObjectName(_fromUtf8("textEdit"))

        self.videoFrame.raise_()
        self.videoFrame_2.raise_()
        self.frame.raise_()
        self.frame_2.raise_()
        self.frame_3.raise_()
        self.frame_4.raise_()
        self.toolButton.raise_()
        self.textEdit.raise_()

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 517, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        self.menuAbout = QtGui.QMenu(self.menubar)
        self.menuAbout.setObjectName(_fromUtf8("menuAbout"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionCapture = QtGui.QAction(MainWindow)
        self.actionCapture.setObjectName(_fromUtf8("actionCapture"))
        self.actionSurf_Detection = QtGui.QAction(MainWindow)
        self.actionSurf_Detection.setObjectName(_fromUtf8("actionSurf_Detection"))
        self.actionAbout = QtGui.QAction(MainWindow)
        self.actionAbout.setObjectName(_fromUtf8("actionAbout"))
        self.menuFile.addAction(self.actionCapture)
        self.menuFile.addAction(self.actionSurf_Detection)
        self.menuAbout.addAction(self.actionAbout)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuAbout.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.toolButton_2.setText(_translate("MainWindow", "Recognition", None))
        self.toolButton.setText(_translate("MainWindow", "FaceTrain", None))
        self.toolButton_3.setText(_translate("MainWindow", "Save", None))
        self.toolButton_4.setText(_translate("MainWindow", "exit", None))
        self.textEdit.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">masukan nama !</p></body></html>", None))
        self.menuFile.setTitle(_translate("MainWindow", "File", None))
        self.menuAbout.setTitle(_translate("MainWindow", "Help", None))
        self.actionCapture.setText(_translate("MainWindow", "Train/Capture", None))
        self.actionSurf_Detection.setText(_translate("MainWindow", "Recognition", None))
        self.actionAbout.setText(_translate("MainWindow", "about", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

